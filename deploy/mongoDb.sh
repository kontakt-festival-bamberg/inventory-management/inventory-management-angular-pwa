#execute on node:
podman pull docker.io/mongodb/mongodb-community-server
#podman run --name mongodb -p 27017:27017 -d docker.io/mongodb/mongodb-community-server:latest

# create mongo conf
sudo mkdir /mongoDB/data/configdb/
sudo mkdir /mongoDB/configdb/
touch /mongoDB/data/configdb/mongo.conf
sudo mkdir /mongoDB/data/log/
sudo chmod -R 775 /mongoDB/
sudo chown -R <USERNAME> /mongoDB/

#write to file
vi /mongoDB/data/configdb/mongo.conf 
#content
systemLog:
  destination: file
  path: /var/log/mongodb/mongod.log
  logAppend: true
storage:
  dbPath: /data/db
net:
  port: 27017
  bindIp: 0.0.0.0
 security:
  authorization: enabled


#start container
#podman run --detach --name mongodb -p 27017:27017 -v /mongoDB/data:/data/db -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=123456 docker.io/mongodb/mongodb-community-server:latest
podman run --detach --name mongodb -p 27017:27017 -v /mongoDB/data:/data/db -v /mongoDB/data/configdb/mongo.conf:/data/configdb/mongo.conf -v /mongoDB/data/log:/var/log/mongodb mongodb/mongodb-community-server:latest -f /data/configdb/mongo.conf
