import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';

import { provideNativeDateAdapter } from '@angular/material/core';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideServiceWorker } from '@angular/service-worker';
import { routes } from './app.routes';
import { InMemoryInventoryItemService } from './services/inventory-item/in-memory-inventory-item.service';
import { InventoryItemService } from "./services/inventory-item/inventory-item.service";
import { InMemoryOwnerService } from './services/owner/in-memory-owner.service';
import { OwnerService } from "./services/owner/owner.service";

export const appConfig: ApplicationConfig = {
  providers: [
    { provide: OwnerService, useClass: InMemoryOwnerService },
    { provide: InventoryItemService, useClass: InMemoryInventoryItemService },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 3000 } },
    provideRouter(routes),
    provideClientHydration(), provideAnimationsAsync(),
    provideServiceWorker('ngsw-worker.js', {
      enabled: !isDevMode(),
      registrationStrategy: 'registerWhenStable:30000'
    }),
    provideNativeDateAdapter()
  ]
};
