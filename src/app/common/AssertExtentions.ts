export abstract class AssertExtentions {
    static ElementFoundById<T>(element: T, id: string): asserts element is NonNullable<T> {
        if (element === undefined || element === null) {
            throw new Error(`Element with id ${id} not found.`);
        }
    }
}