import { Iid } from "./Iid";

export interface IList<T extends Iid> {
    add(element: T): void;
    remove(element: T): void;
    getById(id: string): T;
    toArray(): T[];
    getAll(): IList<T>
}