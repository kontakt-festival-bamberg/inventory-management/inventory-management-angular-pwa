import { CollectionExtentions } from "./CollectionExtentions";
import { IList } from "./IList";
import { Iid } from "./Iid";

export class List<T extends Iid> implements IList<T> {
    private elements: T[] = [];
    constructor(existingData?: T[] | List<T>) {
        if (existingData !== undefined && existingData !== null) {
            if (CollectionExtentions.IsArray(existingData)) {
                this.elements = existingData;
            } else {
                this.elements = existingData.toArray();
            }
        }
    }

    add(element: T): void {
        this.elements.push(element);
    }
    remove(element: T): void {
        const index = this.elements.indexOf(element);
        if (index === -1) {
            //WARNING Improve error hanling. Very hard to debug errors.
            throw new Error(`Element ${element} not found.`);
        }
        if (index > -1) {
            this.elements.splice(index, 1);
        }
    }
    getById(id: string): T {
        //WARN bad non generic implementation
        //WARN improve style: Probably more clean syntax available
        const foundElement = this.elements.find((element) => element.id === id);
        if (foundElement === undefined || foundElement === null) {
            throw new Error(`Element with id ${id} not found.`);
        }
        return foundElement;
    }
    toArray(): T[] {
        const elementsCopy: T[] = [];
        this.elements.forEach(val => elementsCopy.push(Object.assign({}, val)));
        return elementsCopy;
    }
    getAll(): IList<T> {
        throw new Error("Method not implemented.");
    }
}