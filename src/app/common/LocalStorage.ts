import { Injectable } from '@angular/core';
import { AppComponent } from '../app.component';


class LocalStorage implements Storage {
    [name: string]: any;
    readonly length: number = 0;
    clear(): void { }
    getItem(key: string): string | null { return null; }
    key(index: number): string | null { return null; }
    removeItem(key: string): void { }
    setItem(key: string, value: string): void { }
}


@Injectable({
    providedIn: 'root'
})
export class LocalstorageService implements Storage {

    private storage: Storage;

    constructor() {
        this.storage = new LocalStorage();

        AppComponent.isBrowser.subscribe(isBrowser => {
            if (isBrowser) {
                this.storage = localStorage;
            }
            else {
                throw new Error("Executing on server")
            }
        });
    }

    [name: string]: any;

    length: number = 0;

    clear(): void {
        this.storage.clear();
    }

    getItem(key: string): string | null {
        console.debug("getting element: KEY=" + key);
        const val = this.storage.getItem(key);
        console.debug("VAL=" + val)
        return val;
    }

    key(index: number): string | null {
        return this.storage.key(index);
    }

    removeItem(key: string): void {
        return this.storage.removeItem(key);
    }

    setItem(key: string, value: string): void {
        if (key === "" || key === undefined || key === null) {
            throw new Error("Key can not be empty");
        }
        console.debug("setting element: KEY=" + key + "VAL=" + value)
        return this.storage.setItem(key, value);
    }
}