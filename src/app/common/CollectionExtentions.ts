import { Iid } from "./Iid";
import { List } from "./list";

export abstract class CollectionExtentions {
    static IsArray<T extends Iid>(collection: T[] | List<T>): collection is T[] {
        return (collection as T[]).slice !== undefined; //WARNING: Maybe not the best way to check for arrays
    }
}