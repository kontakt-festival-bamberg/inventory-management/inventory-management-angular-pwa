export function nameOf<TObject>(obj: TObject, key: keyof TObject): string;
export function nameOf<TObject>(key: keyof TObject): string;
export function nameOf(key1: any, key2?: any): any {
  return key2 ?? key1;
}

// source https://github.com/dsherret/ts-nameof/issues/121#issuecomment-1520337481