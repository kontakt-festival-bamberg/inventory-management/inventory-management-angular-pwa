export abstract class ErrorExtentions {
    static ThrowElementNotFoundById<T>(element: T, id: string): asserts element is NonNullable<T> {
        throw new Error(`Element with id ${id} not found.`);
    }
}