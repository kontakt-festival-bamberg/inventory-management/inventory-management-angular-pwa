import { Injectable } from "@angular/core";
import { IdService } from "../services/id/id.service";
import { AssertExtentions } from "./AssertExtentions";
import { ErrorExtentions } from "./ErrorExtentions";
import { IList } from "./IList";
import { Iid } from "./Iid";
import { LocalstorageService } from "./LocalStorage";
import { List } from "./list";

@Injectable({
    providedIn: 'root'
})
export class BrowserStoreList<T extends Iid> implements IList<T> {

    private storeName = "";
    private idsKey = "";
    private dummyValue = "dummyValueRequiredToPassElementFoundAssertion"; //WARN Fix this implementation, because it is a burdon for client by adding e.g. a lenght property

    constructor(
        private localStorageService: LocalstorageService,
        private idService: IdService
    ) {
    }
    init(name: string): void {
        const storeId = this.idService.createId();
        this.idsKey = storeId + "Ids";
        this.storeName = storeId + name;
        this.storeIdsStoreInStore();
    }
    add(element: T): void {
        const serialized = JSON.stringify(element);
        this.localStorageService.setItem(this.createKeyByElement(element), serialized);
        this.addId(element.id);
    }
    remove(element: T): void {
        this.localStorageService.removeItem(this.createKeyByElement(element));
        this.removeId(element.id);
    }
    getById(id: string): T {
        const element: T = this.getAndDeserializeElement(this.createKeyById(id));
        return element;
    }
    toArray(): T[] {
        const ids = this.getAndDeserializeStoredIds();
        const storedElements: T[] = []
        ids.forEach(id => {
            if (id !== this.dummyValue) {
                const element = this.getAndDeserializeElement<T>(this.createKeyById(id))
                AssertExtentions.ElementFoundById(element, id);
                storedElements.push(element);
            }
        })
        return storedElements;
    }
    getAll(): IList<T> {
        return new List<T>(this.toArray());
    }

    private storeIdsStoreInStore() {
        const idsList: string[] = [this.dummyValue];
        const serialized = JSON.stringify(idsList);
        this.localStorageService.setItem(this.idsKey, serialized);
    }

    private createKeyByElement(element: T): string {
        return this.storeName + element.id;
    }

    private createKeyById(id: string): string {
        return this.storeName + id;
    }

    private updateIds(newIds: string[]) {
        this.localStorageService.removeItem(this.idsKey);
        this.localStorageService.setItem(this.idsKey, JSON.stringify(newIds));
    }

    private getAndDeserializeElement<T>(key: string): T {
        const storedElementSerialized = this.localStorageService.getItem(key);
        console.debug("getting element:\n" + storedElementSerialized);
        AssertExtentions.ElementFoundById(storedElementSerialized, key);
        const storedElement: T = JSON.parse(storedElementSerialized);
        console.debug("got element:", storedElement);
        return storedElement;
    }

    private getAndDeserializeStoredIds(): string[] {
        const storedIds: string[] = this.getAndDeserializeElement(this.idsKey);
        return storedIds;
    }

    private addId(id: string) {
        const oldIds = this.getAndDeserializeStoredIds();
        oldIds.push(id);
        this.updateIds(oldIds);
    }

    private removeId(id: string) {
        const oldIds = this.getAndDeserializeStoredIds();
        const index = oldIds.indexOf(id, 0);
        //create Array Extensions as soon you now, how to pass array by reference (may be default) private removeById(array: ref string[], id: string){}
        if (index > -1) {
            oldIds.splice(index, 1);
        }
        else {
            ErrorExtentions.ThrowElementNotFoundById(id, id);
        }
        this.updateIds(oldIds);
    }
}