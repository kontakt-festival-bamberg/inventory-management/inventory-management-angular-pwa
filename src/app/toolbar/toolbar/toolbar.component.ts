import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginComponent } from '../../login/login/login.component';
import { SyncStatusComponent } from '../../sync-status/sync-status/sync-status.component';

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [LoginComponent, SyncStatusComponent, MatToolbarModule, MatButtonModule, MatIconModule],
  templateUrl: './toolbar.component.html',
  styleUrl: './toolbar.component.scss'
})
export class ToolbarComponent {
}