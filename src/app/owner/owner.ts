export class Owner {
    constructor(
        public id: string,
        public name: string
    ) {}
}
