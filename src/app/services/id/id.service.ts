import { Injectable } from '@angular/core';
import { md5 } from "js-md5";

@Injectable({
  providedIn: 'root'
})
export class IdService {
  counter = 0; //to prevent same hash at same time
  constructor() { }

  createId(): string{
    this.counter++;
    const hash = md5(`${Date.now}`+ this.counter)
    //console.warn(hash);
    //console.error("counter: "+ this.counter);
    return hash;
  }
}
