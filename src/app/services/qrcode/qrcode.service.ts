import { Injectable } from '@angular/core';
import QRCode from 'qrcode';

@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  constructor() { }

  generateQrCode(id: string) {
    //do this part first, then add the image picker and camera taker
    //QRCode.toBuffer()
    const path = '~/test-qr-code.png';
    QRCode.toFile(path, id)
      .then(foo => {
        console.log(foo);
        console.log('done')
      })
      .catch(err => {
        console.error(err)
      })
  }
}
