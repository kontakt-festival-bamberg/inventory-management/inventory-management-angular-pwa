import { TestBed } from '@angular/core/testing';

import { InMemoryOwnerService } from './in-memory-owner.service';

describe('InMemoryOwnerService', () => {
  let service: InMemoryOwnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InMemoryOwnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
