import { Injectable } from '@angular/core';
import { BrowserStoreList } from '../../common/BrowserStoreList';
import { Owner } from '../../owner/owner';
import { IdService } from '../id/id.service';
import { OwnerService } from './owner.service';

@Injectable({
  providedIn: 'root'
})
export class InMemoryOwnerService implements OwnerService {

  constructor(
    private idService: IdService,
    private owners: BrowserStoreList<Owner>
  ) {
    this.owners.init("Owners");
  }

  create(name: string): Owner {
    const owner = new Owner(this.idService.createId(), name);
    this.owners.add(owner);
    return owner;
  }

  delete(owner: Owner): void {
    this.owners.remove(owner);
  }
}
