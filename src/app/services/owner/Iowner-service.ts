import { Owner } from "../../owner/owner";

export interface IOwnerService {
    delete (owner: Owner): void
    create (name: string): Owner
}
