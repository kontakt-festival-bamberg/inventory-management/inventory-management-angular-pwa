import { TestBed } from '@angular/core/testing';

import { SyncStatusService } from './sync-status.service';

describe('SyncStatusService', () => {
  let service: SyncStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SyncStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
