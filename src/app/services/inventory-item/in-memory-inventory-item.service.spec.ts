import { TestBed } from '@angular/core/testing';

import { InMemoryInventoryItemService } from './in-memory-inventory-item.service';

describe('InMemoryInventoryItemService', () => {
  let service: InMemoryInventoryItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InMemoryInventoryItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
