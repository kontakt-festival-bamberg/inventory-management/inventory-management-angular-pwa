import { Injectable } from '@angular/core';
import { BrowserStoreList } from '../../common/BrowserStoreList';
import { List } from '../../common/list';
import { InventoryItem } from '../../inventory-item/inventory-item';
import { Owner } from '../../owner/owner';
import { IdService } from '../id/id.service';
import { OwnerService } from '../owner/owner.service';
import { IInventoryItemService } from './Iinventory-item-service';
import { InventoryItemCreationDto } from './dtos/inventory-item-creation-dto';

@Injectable({
  providedIn: 'root'
})
export class InMemoryInventoryItemService implements IInventoryItemService {

  constructor(
    private idService: IdService,
    private ownerService: OwnerService,
    private items: BrowserStoreList<InventoryItem>
  ) {
    this.items.init("InventoryItems");
    this.createExampleInventoryItems();
  }
  getAll(): List<InventoryItem> {
    return new List<InventoryItem>(this.items.toArray())
  }
  delete(inventoryItem: InventoryItem): void {
    this.items.remove(inventoryItem);
    console.info("deleted inventory item " + inventoryItem.id);
  }
  create(data: InventoryItemCreationDto): InventoryItem {
    const createdId = this.idService.createId();
    data.id = createdId;
    const item = (new InventoryItem(data));
    this.items.add(item);
    console.info("created inventory item " + item.id);
    console.debug(item);
    return item;
  }
  getById(id: string): InventoryItem {
    return this.items.getById(id);
  }

  private createExampleInventoryItems() {
    const ownerJohannes = this.ownerService.create("Johannes Helmut Sirsch");
    const exampleItemDto1 = new InventoryItemCreationDto(
      this.idService.createId(),
      "Label Printer",
      new Owner(this.idService.createId(),
        "Backspace"), new Date(2024, 6, 1)
    );
    const exampleItemDto2 = new InventoryItemCreationDto(
      this.idService.createId(),
      "RaspberryPi 4",
      ownerJohannes,
      new Date(2024, 6, 1),
      "Don't remove power, otherwise application will no longer function."
    );
    const exampleItemDto3 = new InventoryItemCreationDto(
      this.idService.createId(),
      "Green Tea Cup",
      ownerJohannes
    );

    const exampleInventoryItems = [
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
      new InventoryItem(exampleItemDto1),
      new InventoryItem(exampleItemDto2),
      new InventoryItem(exampleItemDto3),
    ];
    exampleInventoryItems.forEach(item => {
      this.items.add(item);
    });
  }
}
