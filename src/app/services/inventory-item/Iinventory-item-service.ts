import { List } from "../../common/list";
import { InventoryItem } from "../../inventory-item/inventory-item";
import { InventoryItemCreationDto } from "./dtos/inventory-item-creation-dto";

export interface IInventoryItemService {
    getById(id: string): InventoryItem
    delete(inventoryItem: InventoryItem): void
    create(data: InventoryItemCreationDto): InventoryItem
    getAll(): List<InventoryItem>
}
