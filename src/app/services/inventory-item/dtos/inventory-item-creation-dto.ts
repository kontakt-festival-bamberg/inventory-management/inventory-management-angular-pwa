import { Owner } from "../../../owner/owner";

export class InventoryItemCreationDto {
    constructor(
        public id?: string,
        public name?: string,
        public owner?: Owner,
        public returnDate: Date = new Date(),
        public comment: string = "add comment"
    ) { }

}