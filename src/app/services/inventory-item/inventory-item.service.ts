import { Injectable } from '@angular/core';
import { List } from '../../common/list';
import { InventoryItem } from '../../inventory-item/inventory-item';
import { IInventoryItemService } from './Iinventory-item-service';
import { InventoryItemCreationDto } from './dtos/inventory-item-creation-dto';

@Injectable({
  providedIn: 'root'
})
export class InventoryItemService implements IInventoryItemService {

  constructor() { }
  getAll(): List<InventoryItem> {
    throw new Error('Method not implemented.');
  }

  getById(id: string): InventoryItem {
    throw new Error('Method not implemented.');
  }
  delete(inventoryItem: InventoryItem): void {
    throw new Error('Method not implemented.');
  }
  create(data: InventoryItemCreationDto): InventoryItem {
    throw new Error('Method not implemented.');
  }
}
