import { Component } from '@angular/core';

@Component({
  selector: 'app-default-welcome',
  standalone: true,
  imports: [],
  templateUrl: './default-welcome.component.html',
  styleUrl: './default-welcome.component.scss'
})
export class DefaultWelcomeComponent {
  title = 'inventory-management-angular-pwa';
}
