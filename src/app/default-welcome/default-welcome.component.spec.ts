import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultWelcomeComponent } from './default-welcome.component';

describe('DefaultWelcomeComponent', () => {
  let component: DefaultWelcomeComponent;
  let fixture: ComponentFixture<DefaultWelcomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DefaultWelcomeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DefaultWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
