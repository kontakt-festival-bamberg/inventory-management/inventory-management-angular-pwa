import { animate, state, style, transition, trigger } from "@angular/animations";
import { NgSwitch, NgSwitchCase } from '@angular/common';
import { Component } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule, ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { SyncStatusService } from "../../services/sync-status/sync-status.service";
import { SyncStatus } from './sync-status';

@Component({
  selector: 'app-sync-status',
  standalone: true,
  imports: [NgSwitch, NgSwitchCase, MatIconModule, MatProgressSpinnerModule],
  templateUrl: './sync-status.component.html',
  styleUrl: './sync-status.component.scss',
  animations: [
    trigger('enterTrigger', [
      state('fadeIn', style({
        opacity: '1'
      })),
      transition('void => *', [style({ opacity: '0' }), animate('200ms')])
    ])
  ]
})
export class SyncStatusComponent {
  syncStatusType = SyncStatus;
  syncStatus = SyncStatus.waitingForNetwork;
  offlineColor: ThemePalette = 'warn';
  spinnerColor: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'indeterminate';
  //value = 50; //use in future in determined mode to show upload percentage

  constructor(private syncStatusService: SyncStatusService) {
  }
}
