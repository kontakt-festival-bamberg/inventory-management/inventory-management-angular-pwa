export enum SyncStatus {
    waitingForNetwork,
    syncing,
    synced
};