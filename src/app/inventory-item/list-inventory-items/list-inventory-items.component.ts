import { Component } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { InventoryItemService } from '../../services/inventory-item/inventory-item.service';
import { IInventoryItem } from '../inventory-item';

@Component({
  selector: 'app-list-inventory-items',
  standalone: true,
  imports: [MatInputModule, MatFormFieldModule, MatTableModule],
  providers: [],
  templateUrl: './list-inventory-items.component.html',
  styleUrl: './list-inventory-items.component.scss'
})
export class ListInventoryItemsComponent {

  //WARN change to dynamic colum names
  displayedColumns: string[] = ['id', 'name', 'owner', 'return date', 'comment'];
  dataSource: MatTableDataSource<IInventoryItem>;
  constructor(
    private inventoryItemService: InventoryItemService
  ) {
    const inventoryItems = inventoryItemService.getAll().toArray();
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource<IInventoryItem>(inventoryItems);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    //We don't have a paginator yet. It may be required, if the dataset gets to big.
    // if (this.inventoryItems.paginator) {
    //   this.inventoryItems.paginator.firstPage();
    // }
  }
}
