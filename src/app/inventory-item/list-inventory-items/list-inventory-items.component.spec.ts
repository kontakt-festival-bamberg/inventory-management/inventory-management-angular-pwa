import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListInventoryItemsComponent } from './list-inventory-items.component';

describe('ListInventoryItemsComponent', () => {
  let component: ListInventoryItemsComponent;
  let fixture: ComponentFixture<ListInventoryItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListInventoryItemsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListInventoryItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
