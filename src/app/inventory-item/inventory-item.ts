import { Owner } from "../owner/owner";
import { InventoryItemCreationDto } from "../services/inventory-item/dtos/inventory-item-creation-dto";

export interface IInventoryItem {
    id: string;
    name?: string;
    owner?: Owner;
    returnDate: Date;
    comment: string;
}

export class InventoryItem implements IInventoryItem {

    public id: string;
    public name: string;
    public owner: Owner;
    public returnDate: Date = new Date();
    public comment: string = "add comment";

    constructor(data: InventoryItemCreationDto) {
        if (data.id == null || data.id === "") {
            throw new Error("ID is required! Use e.g. IdCreationService.");
        }

        if (data.name == null || data.name === "") {
            throw new Error("Name is required!");
        }

        if (data.owner == null || data.owner === undefined) {
            throw new Error("Owner is required!");
        }


        this.id = data.id;
        this.name = data.name;
        this.owner = data.owner;
        this.returnDate = data.returnDate;
        this.comment = data.comment;
    }
}
