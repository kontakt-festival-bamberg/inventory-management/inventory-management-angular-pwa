import { BreakpointObserver } from '@angular/cdk/layout';
import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from "@angular/material/card";
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule, StepperOrientation } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GenerateRoute } from '../../app.routes';
import { InventoryItemCreationDto } from '../../services/inventory-item/dtos/inventory-item-creation-dto';
import { InventoryItemService } from '../../services/inventory-item/inventory-item.service';
import { OwnerService } from '../../services/owner/owner.service';
import { InventoryItem } from '../inventory-item';


@Component({
  selector: 'app-add-inventory-item',
  standalone: true,
  imports: [
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    AsyncPipe,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
  ],
  providers: [provideNativeDateAdapter()],
  templateUrl: './add-inventory-item.component.html',
  styleUrl: './add-inventory-item.component.scss'
})
export class AddInventoryItemComponent {
  stepperOrientation: Observable<StepperOrientation>;
  firstFormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    owner: ['', Validators.required]
  });
  secondFormGroup = this._formBuilder.group({
    returnDate: ['', Validators.required],
  });
  thirdFormGroup = this._formBuilder.group({
    comment: ['', Validators.required],
  });
  fourthFormGroup = this._formBuilder.group({
    fourthCtrl: ['', Validators.required],
  });

  selected: Date | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _formBuilder: FormBuilder,
    breakpointObserver: BreakpointObserver,
    private inventoryItemService: InventoryItemService,
    private ownerService: OwnerService
  ) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));
  }

  onStepChange(event: any) {
    //WARN remove any type
    //if (event.selectedIndex === 5) {
    //  this.onLastStepDone();
    //}
    console.warn("Use submit button instead!");
  }

  onLastStepDone() {
    const item = this.createItem();
    console.log(item);
    //this.startSync(); // start spinner here? responsibility of item service?
    this.redirectToEdit(item.id); //? Does this make sense?
    //Does someone add another component?
    //Show data with QR code first?
  }

  // createNewItem() {
  //   console.log("Creating new Item to start directly adding a new item and editing it afterwards to not force the user to click on submit.");
  // therefore add rxjs library and automatically create change updates on value change or field unfocus.
  // }

  createItem(): InventoryItem {
    return this.inventoryItemService.create(
      new InventoryItemCreationDto(
        undefined,
        this.firstFormGroup.value.name ? this.firstFormGroup.value.name : undefined,
        this.firstFormGroup.value.owner ? this.ownerService.create(this.firstFormGroup.value.owner) : undefined,
        this.secondFormGroup.value.returnDate ? new Date(this.secondFormGroup.value.returnDate) : undefined,
        this.thirdFormGroup.value.comment ? this.thirdFormGroup.value.comment : undefined
      ));
  }

  redirectToEdit(id: string) {
    this.router.navigate([GenerateRoute.editInventoryItemById(id)]);
  }

  startSync() {
    throw new Error('Function not implemented.');
  }
}

