import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { GenerateRoute } from '../../app.routes';
import { InventoryItemService } from "../../services/inventory-item/inventory-item.service";
import { InventoryItem } from '../inventory-item';

@Component({
  selector: 'app-edit-inventory-item',
  standalone: true,
  imports: [],
  templateUrl: './edit-inventory-item.component.html',
  styleUrl: './edit-inventory-item.component.scss'
})
export class EditInventoryItemComponent implements OnInit, OnDestroy {
  id: string = "";
  private sub: any;
  public inventoryItem: InventoryItem | undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private inventoryItemService: InventoryItemService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      // In a real app: dispatch action to load the details here.
    });
    if (this.id === '0') {
      this.notFoundRedirect();
    } else {
      try {
        this.loadItem();
      } catch (error) {
        console.warn("Add scan retry here in case of scann failure!");
        this.notFoundRedirect();
      }
    }
  }
  notFoundRedirect() {
    this.snackBar.open(`Item not found! Please create a new Item!`);
    this.router.navigate([GenerateRoute.addInventoryItemTemplate]);
  }

  private loadItem() {
    try {
      console.warn("in EditInventoryItemComponent before this.inventoryItemService.getById!!!");
      this.inventoryItem = this.inventoryItemService.getById(this.id);
      console.debug("here!!!");
    } catch (error) {
      console.error(error + "in edit component");
      this.snackBar.open(`Item with id ${this.id} not found.`);
      this.router.navigate(['/']);
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // createNewItem() {
  //   this.inventoryItem = this.inventoryItemService.create();
  //   this.router.navigate(['../' + this.inventoryItem.id], { relativeTo: this.route });
  //   this.snackBar.open("Created new inventory item!", "Undo").onAction().subscribe(() => this.undoCreateItemAction());
  // }
  createNewItem() {
    this.router.navigate([GenerateRoute.addInventoryItemTemplate]);
    this.snackBar.open("Created new inventory item!", "Undo").onAction().subscribe(() => this.undoCreateItemAction());
  }


  undoCreateItemAction() {
    this.deleteItemAndRedirect();
  }

  deleteItemAndRedirect() {
    if (this.inventoryItem === undefined) {
      return;
    }
    this.inventoryItemService.delete(this.inventoryItem);
    this.router.navigate(['/']);
  }
}
