import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  constructor(private snackBar: MatSnackBar) {
  }

  onLogin() {
    this.snackBar.open(`Not Implemented yet.`);
  }

}
