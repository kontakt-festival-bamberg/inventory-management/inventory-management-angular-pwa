import { Routes } from '@angular/router';
import { DefaultWelcomeComponent } from './default-welcome/default-welcome.component';
import { HomeComponent } from './home/home.component';
import { AddInventoryItemComponent } from './inventory-item/add-inventory-item/add-inventory-item.component';
import { EditInventoryItemComponent } from './inventory-item/edit-inventory-item/edit-inventory-item.component';

const inventoryItem = "inventory-item";


export abstract class GenerateRoute {
    static editInventoryItemById(id: string): string {
        return `${inventoryItem}/edit/${id}`;
    }
    static editInventoryItemByIdTemplate = `${inventoryItem}/edit/:id`;
    static addInventoryItemTemplate = `${inventoryItem}/add`;
};

export const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'default-welcome',
        component: DefaultWelcomeComponent
    },
    {
        path: GenerateRoute.addInventoryItemTemplate,
        component: AddInventoryItemComponent
    },
    {
        path: GenerateRoute.editInventoryItemByIdTemplate,
        component: EditInventoryItemComponent
    }
];
