serverDir="../dist/inventory-management-angular-pwa/server/"
cp shell.nix $serverDir

userNixStoreCache="$HOME/nix/store/"
useCache=true

# REFACTOR: Extract local nix store cache code to separate .sh script

# caching not working right now, fix later
if false; then #  [ -d $userNixStoreCache ]; then
  podman run -it -p 8080:8080 -v $userNixStoreCache:/nix/store/ -v $(pwd)/../dist/inventory-management-angular-pwa/server/:/server nixos/nix
else
  echo "User nix store cache not found at ~/nix/store/! Going forward without caching."
  useCache=false
  podman run -it -p 8080:8080 -v $(pwd)/../dist/inventory-management-angular-pwa/server/:/server nixos/nix
fi

#USE THIS SYNTAX in future to remove code dupliation
#podman run -it -p 8080:8080 ${userNixStoreCache:""} -v $(pwd)/../dist/inventory-management-angular-pwa/server/:/server nixos/nix